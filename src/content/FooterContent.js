import photo1 from '../assets/Images/footer-a1.jpg';
import photo2 from '../assets/Images/footer-a2.jpg';
import photo3 from '../assets/Images/footer-a3.jpg';

export const FooterContent = {
    first: [
        {
            Title: 'Magna Mollis Ulricies',
            Date: '12 NOV 2017',
            Comments: 4,
            Photo: photo1
        },
        {
            Title: 'Ornare Nullam Risus',
            Date: '12 NOV 2017',
            Comments: 4,
            Photo: photo2
        },
        {
            Title: 'Euismod Nullam',
            Date: '12 NOV 2017',
            Comments: 4,
            Photo: photo3
        }
    ],
    second:{
        LifeStyle: 21,
        Photo: 19,
        Journal: 16,
        Works: 7,
        StillLife: 9,
        Travel: 17
    },
    third: {
        Address1: 'Moonshine St. 14/05',
        Address2: 'Nis, Serbia',
        Email: 'info@email.com',
        Number: '+00(123) 456 789'
    }
}