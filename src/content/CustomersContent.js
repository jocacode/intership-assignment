import customer1 from '../assets/Images/customer-t1.jpg';
import customer2 from '../assets/Images/customer-t2.jpg';
import customer3 from '../assets/Images/customer-t3.jpg';
// import customer4 from '../assets/Images/customer-t4.jpg';
// import customer5 from '../assets/Images/customer-t5.jpg';
// import customer6 from '../assets/Images/customer-t6.jpg';
import LoremIpsum from 'react-lorem-ipsum';
import React from 'react';


export const CustomersContent  = [
    {
        Text: ( <LoremIpsum p={1} avgWordsPerSentence={1} ></LoremIpsum> ),
        Photo: customer1,
        Name: 'Connor',
        Surname: 'Gibson',
        Position: 'Financial Analyst'
    },
    {
        Text: ( <LoremIpsum p={1} avgWordsPerSentence={1} ></LoremIpsum> ),
        Photo: customer2,
        Name: 'Coriss',
        Surname: 'Ambady',
        Position: 'Marketing Specialist'
    },
    {
        Text: ( <LoremIpsum p={1} avgWordsPerSentence={1} ></LoremIpsum> ),
        Photo: customer3,
        Name: 'Barclay',
        Surname: 'Widerski',
        Position: 'Sales Manager'
    }
]

export const CustomersTitle = {
    smallTitle: 'CUSTOMER STORIES',
    bigTitle: 'Customer satisfaction is our major goal. See what our customers are saying about us.'
}