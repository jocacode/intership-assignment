export const TalkContent = {
    Type: 'LET`S TALKS',
    Title: 'Let`s make something great together If you got any questions, don`t hesitate to get in touch with us.'
}

export const EmailContent = {
    Type: 'ANALYZE NOW',
    Title: 'Wonder how much faster website can go? Easily check your SEO Score now.'
}