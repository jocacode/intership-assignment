import LoremIpsum from 'react-lorem-ipsum';
import React from 'react';
export const firstPhotoSection = {
    Type: 'WHY CHOOSE US?',
    Title: 'Why is Search Engine Optimization importan for your business?',
    Content: ( <LoremIpsum p={1} avgWordsPerSentence={3} ></LoremIpsum> ),
    List: ['Aenean eu leo ornare.','Nullam quis risus molis.','Donec elit non mi gravida.','Fusce dapibus cursus commodo.']
};
export const secondPhotoSection =  {
        Type: 'OUR PERSONALIZED SOLUTIONS',
        Title: 'Just sit and relax while we take care of you business needs',
        Content: ( <LoremIpsum p={1} avgWordsPerSentence={3} ></LoremIpsum> ),
        List: ['Aenean eu leo ornare.','Nullam quis risus molis.','Donec elit non mi gravida.','Fusce dapibus cursus commodo.']
};

export const ProcessSection = {
    Type: 'OUR PROCESS',
    Title: 'We bring solutions to make life easier for our customers',
    Cards: [
        {
            Type: 'Ideas', Title: 'Collect Ideas', 
            Content: 'Nulla vitae elit libero, a pharetra augue. Donec id elit non mi porta gravida at eget metus. Cras justo.'
        },
        {
            Type: 'Data', Title: 'Data Analysis',
            Content: 'Nulla vitae elit libero, a pharetra augue. Donec id elit non mi porta gravida at eget metus. Cras justo.'
        },
        {
            Type: 'Magic', Title: 'Magic Touch',
            Content: 'Nulla vitae elit libero, a pharetra augue. Donec id elit non mi porta gravida at eget metus. Cras justo.'
        },
    ]
}