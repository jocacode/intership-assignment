import React from 'react';
import LoremIpsum from 'react-lorem-ipsum';

export const PricingTitle = {
    Type: 'OUR PRICING',
    Title: 'We offer great prices, premium products and quality service for your bisiness.',
    Content:  ( <LoremIpsum p={1} avgWordsPerSentence={3} ></LoremIpsum> )
}

export const PriceCards = [
    {
        Type: 'Basic Plan',
        Price: 9,
        Projects: 1,
        API: '100K',
        Storage: '100MB'
    },
    {
        Type: 'Premium Plan',
        Price: 19,
        Projects: 5,
        API: '100K',
        Storage: '200MB'
    },
]