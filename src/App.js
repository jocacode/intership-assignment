import React, { Component } from 'react';
import Layout from './components/Layout/Layout';
import Aux from '../src/HOC/Auxilary';
import {Home} from './components/Home/Home';
import {WhatWeDo} from './components/WWD/WhatWeDo';
import {PhotoSection} from './components/Title-Photo Section/PhotoSection';
import firstPhoto from './assets/Images/concept8.png';
import {firstPhotoSection} from './content/photoSectionContent';
import secondPhoto from './assets/Images/concept3.png';
import {secondPhotoSection} from './content/photoSectionContent';
import {OurProcess} from './components/OurProcess/OurProcess';
import ProcessPhoto from './assets/Images/concept1.png';
import {ProcessSection} from './content/photoSectionContent';
import {Customers} from './components/Customers/Customers';
import {CustomersContent} from './content/CustomersContent';
import {CustomersTitle} from './content/CustomersContent'
import {Pricing} from './components/Pricing/Pricing';
import {PricingTitle} from './content/PricingContent';
import {PriceCards} from './content/PricingContent';
import {Talk} from './components/Talk/Talk';
import TalkPhoto from './assets/Images/concept12.png';
import {TalkContent} from './content/TalkContent';
import {Email} from './components/Email/Email';
import {EmailContent} from './content/TalkContent';
import {Footer} from './components/Footer/Footer';
import {FooterContent} from './content/FooterContent';

class App extends Component {
  state = {
    email: ''
  }

  setEmailHandler = (e) => {
    this.setState({email: e.target.value});
  }
  alertEmailHandler = () => {
    if(this.state.email !== '')
      alert('Your email: ' + this.state.email);
    alert('You need to enter email address!')
  }
  render(){
    return (
      <Aux>
        <Layout>
            <Home/>
            <WhatWeDo/>
            <PhotoSection 
                photo={firstPhoto} 
                content={firstPhotoSection}/>
              <OurProcess 
                photo={ProcessPhoto}
                content={ProcessSection}/>
             <PhotoSection 
                photo={secondPhoto} 
                content={secondPhotoSection}/>
              <Customers 
                  customers={CustomersContent}
                  title={CustomersTitle} />
              <Pricing title={PricingTitle} cards={PriceCards}/>
              <Talk photo={TalkPhoto} content={TalkContent}/>
              <Email 
                  change={this.setEmailHandler} 
                  content={EmailContent}
                  click={this.alertEmailHandler}/>
                <Footer content={FooterContent}/>
        </Layout>
      </Aux>
    );
  }
}

export default App; 
