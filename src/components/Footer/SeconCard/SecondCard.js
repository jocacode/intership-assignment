import React from 'react'
import styles from  './SecondCard.module.css';
import { Button } from '../../UI/Button/Button';

export const SecondCard = (props) => {
    console.log(props.categories);
    console.log(Object.keys(props.categories));
    return (
        <div className={styles.SecondCard}>
            <h3>Tags</h3>
            <div style={{display: 'flex',flexDirection:'row'}}>
                <Button width='120px' text="STILL LIFE"/>
                <Button width='120px' text="URBAN"/>
            </div>
            <div style={{display: 'flex', flexDirection:'row'}}>
                <Button width='120px' text="NATURE"/>
                <Button width='120px' text="LANDSCAPE"/>
            </div>
            <div className={styles.Categories}>
                <h3>Categories</h3>
                <div className={styles.ListElements}> 
                    {Object.keys(props.categories).map((categorie, index) =>{
                        return  <li key={index}>{categorie}({props.categories[categorie]})</li>
                    })}
                </div>
            </div>
        </div>
    )
}
