import React from 'react'
import styles from './FirstCard.module.css'

export const FirstCard = (props) => {
    return (
        <div className={styles.FirstCard}>
            <h3>Popular Posts</h3>
            {props.list.map((item,index) => {
                return(
                    <div className={styles.Card} key={index}>
                        <img src={item.Photo} alt="Popular Post"/>
                        <div className={styles.CardTitle}>
                            <h4>{item.Title}</h4>
                            <p><span role="img">🕓</span>{item.Date} . <span role="img">🗨</span>{item.Comments}</p>
                        </div>
                    </div>
                )
            })}
        </div>
    )
}
