import React from 'react'
import styles from './Footer.module.css';
import { FirstCard } from './FirstCard/FirstCard';
import { SecondCard } from './SeconCard/SecondCard';
import { ThirdCard } from './ThirdCard/ThirdCard';
import {FourthCard} from './FourthCard/FourthCard';

export const Footer = (props) => {
    return (
        <div className={styles.Footer}>
            <div className={styles.Container}>
               <FirstCard list={props.content.first}/>
               <SecondCard categories={props.content.second}/>
               <ThirdCard contact={props.content.third}/>
               <FourthCard/>
            </div>
        </div>
    )
}
