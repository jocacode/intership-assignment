import React from 'react'
import styles from './FourthCard.module.css';

export const FourthCard = () => {
    return (
        <div className={styles.FourthCard}>
            <h3>Learn More</h3>
            <p>About as</p>
            <p>Our Story</p>
            <p>Projects</p>
            <h3>Need Help?</h3>
            <p>Support</p>
            <p>Get Started</p>
            <p>Contact us</p>
        </div>
    )
}
