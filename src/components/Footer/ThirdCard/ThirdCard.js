import React from 'react'
import styles from './ThirdCard.module.css';
import twitter from '../../../assets/Images/twitter.png';
import fb from '../../../assets/Images/fb.png';
import insta from '../../../assets/Images/instagram.jpg';
import vimeo from '../../../assets/Images/vimeo.png';
import yt from '../../../assets/Images/yt.png';

export const ThirdCard = (props) => {
    return (
        <div className={styles.ThirdCard}>
            <h3>Get in Touch</h3>
            <p>{props.contact.Address1}</p>
            <p>{props.contact.Address2}</p>
            <br/>
            <p>{props.contact.Email}</p>
            <p>{props.contact.Number}</p>
            <h3>Elsewhere</h3>
            <div style={{display: 'flex', flexDirection: 'row'}}>
                <img scr={twitter} alt="twitter"/>
                <img scr={fb} alt="facebook"/>
                <img scr={insta} alt="instagram"/>
                <img scr={vimeo} alt="vimeo"/>
                <img scr={yt} alt="youtube"/>
            </div>
        </div>
    )
}
