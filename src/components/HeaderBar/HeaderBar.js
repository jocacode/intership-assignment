import React from 'react'
import styles from './HeaderBar.module.css';
import {NavigationItems} from '../NavigationItems/NavigationItems';
import {MenuButton} from '../UI/MenuButton/MenuButton';
import Aux from '../../HOC/Auxilary';
import logo from '../../assets/Images/logo.png';

export const HeaderBar = (props) => {
    return (
        <Aux>
            <header className={styles.Header}>
                <div className={styles.MenuButton}> 
                    <MenuButton click={props.showSideDrawer} />
                </div>
                <div className={styles.Navigation}>
                    <div className={styles.Logo}>
                        <img src={logo} alt="Logo"/>
                    </div>
                    <div className={styles.Nav}>
                        {props.showSideDrawer ?  
                        <nav className={styles.DesktopOnly}>
                                <NavigationItems/>
                            </nav> : null}
                    </div>
                </div>
            </header>
            {props.children}
        </Aux>
        )
}
