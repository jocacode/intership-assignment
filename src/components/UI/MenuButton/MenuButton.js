import React from 'react'
import styles from './MenuButton.module.css';


export const MenuButton = (props) => {
    return (
            <div className={styles.MenuButton} onClick={props.click}>
                <button className={styles.Button}>
                    <div className={styles.Line}></div>
                    <div className={styles.Line}></div>
                    <div className={styles.Line}></div>
                </button>
            </div>
    )
}
