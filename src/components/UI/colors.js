export const Colors = {
    Blue: '#5ca7db',
    Black: '#404040',
    Grey: '#999999',
    DarkGrey: '#606060',
    Yellow: '#f5c463',
    Green: '#58b39'
}