import React from 'react'
import styles from './Title.module.css';

export const Title = (props) => {
    return (
        <div className={styles.Title}>
            <h5 style={{textAlign: props.align}}>{props.smallTitle}</h5>
            <h3 style={{textAlign: props.align}}>{props.bigTitle}</h3>
        </div>
    )
}
