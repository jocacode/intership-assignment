import React from 'react'
import styles from './Button.module.css'

export const Button = (props) => {
    return (
        <div className={styles.Button}>
            <button style={{width: props.width}} onClick={props.click}>
                <p style={{color: props.color}}>{props.text}</p>
            </button>
        </div>   
    )
}
