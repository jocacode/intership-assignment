import React from 'react'
import styles from './Input.module.css';

export const Input = (props) => {
    return (
        <div className={styles.Input}>
            <input onChange={props.change} placeholder={props.placeholder} type="text"/>
        </div>
    )
}
