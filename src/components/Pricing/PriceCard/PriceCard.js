import React from 'react'
import styles from './PriceCard.module.css';
import {Button} from '../../UI/Button/Button';

export const PriceCard = (props) => {
    return (
        <div className={styles.PriceCard}>
            <div style={{display: 'flex'}}>
                <p>$</p>
                    <h5>{props.card.Price}</h5>
                <p> / month</p>
            </div>
            <h4>{props.card.Type}</h4>
            <p><strong>{props.card.Projects}</strong> Project</p>
            <p><strong>{props.card.API}</strong> API Access</p>
            <p><strong>{props.card.Storage}</strong> Storage</p>
            <p>Custom Cloud Services</p>
            <p>Weekly Reports</p>   
            <p>7/24 Support</p>
            <div style={{marginTop: '50px'}}>
                <Button width="170px" color="white" text="CHOOSE PLAN"/>
            </div>
        </div>
    )
}
