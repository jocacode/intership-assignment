import React from 'react'
import styles from './Pricing.module.css';
import {PriceCard} from './PriceCard/PriceCard';

export const Pricing = (props) => {
    return (
        <div className={styles.Pricing} id="PRICING">
            <div className={styles.Container}>
                <div className={styles.Title}>
                    <h5>{props.title.Type}</h5>
                    <h3>{props.title.Title}</h3>
                    {props.title.Content}
                </div>
                <div className={styles.Cards}>
                    {props.cards.map((item,index) => {
                        return <PriceCard key={index} card={item}/>
                    })}
                </div>
            </div>
        </div>
    )
}
