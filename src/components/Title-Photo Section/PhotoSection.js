import React from 'react'
import styles from './PhotoSection.module.css';
import LoremIpsum from 'react-lorem-ipsum';
import Correct from '../../assets/Images/correct.png';

export const PhotoSection = (props) => { 
    const reverse = props.reverse ? {flexDirection: 'row-reverse'} : null;
    return (
        <div className={styles.PhotoSection}>
            <div className={styles.PhotoContainer} style={reverse}>
                <div className={styles.Photo}>
                    <img src={props.photo} alt='people'/>
                </div>
                <div className={styles.Title}>
                    <h4>{props.content.Type}</h4>
                    <h3>{props.content.Title}</h3>
                    <LoremIpsum p={1} avgWordsPerSentence={3} ></LoremIpsum>
                    <ul>
                        {props.content.List.map((item, index) => {
                            return <li key={index}><img src={Correct} alt='correct'/>{item}</li>
                        })}
                    </ul>
                </div>
            </div>  
        </div>
    )
}
