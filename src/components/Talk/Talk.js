import React from 'react'
import styles from './Talk.module.css';
import LoremIpsum from 'react-lorem-ipsum';
import {Title} from '../UI/Title/Title';
import { Button } from '../UI/Button/Button';

export const Talk = (props) => {
    return (
        <div className={styles.Talk}>
            <div className={styles.Container}>
                <div className={styles.Photo}>
                    <img src={props.photo} alt='people'/>
                </div>
                <div className={styles.TalkTitle}>
                    <Title align="left" smallTitle={props.content.Type} bigTitle={props.content.Title}/>
                    <LoremIpsum p={1} avgWordsPerSentence={3} ></LoremIpsum>
                    <Button color="white" width="150px" text="CONTACT US"/>
                </div>
            </div>
        </div>
    )
}
