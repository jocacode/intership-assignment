import React from 'react'
import styles from './Rocket.module.css';
import rocket2 from '../../../assets/Images/rocket2.png';
import rocket3 from '../../../assets/Images/rocket3.png';
import rocket4 from '../../../assets/Images/rocket4.png';

export const Rocket = (props) => {
    return (
        <div className={styles.Rocket}>
                <img className={styles.rocket3} src={rocket3} alt="rocket3" />
                <img className={styles.rocket4} src={rocket4} alt="rocket4" />
                <img className={styles.rocket2} src={rocket2} alt="rocket2" />
        </div>
    )
}
