import React from 'react'
import styles from './HomeText.module.css';
import {Button} from '../../UI/Button/Button';

export const HomeText = (props) => {
    return (
        <div className={styles.HomeText}>
            <h1>Grow Your Bussines with Our Marketing Solutions</h1>
            <p>We help our client to increase their website traffic, rankings visibility in search results.</p>
            <div className={styles.Button}><Button text="TRY IT FOR FREE"/></div>
        </div>
    )
}
