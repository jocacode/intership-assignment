import React from 'react'
import styles from './Home.module.css';
import {HomeText} from './HomeText/HomeText';
import {Rocket} from './Rocket/Rocket';
import rocket1 from '../../assets/Images/rocket1.png';

export const Home = (props) => {
    return (
        <div className={styles.Home} id="HOME">
            <div className={styles.Content}>
                <HomeText/>
                <Rocket/>
            </div>
            <div className={styles.Cloud}>
                <img className={styles.rocket1} src={rocket1} alt="rocket1" />
            </div>
        </div>
    )
}
