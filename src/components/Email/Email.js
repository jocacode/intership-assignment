import React from 'react'
import styles from './Email.module.css';
import { Title } from '../UI/Title/Title';
import { Button } from '../UI/Button/Button';
import {Input} from '../UI/Input/Input';
import rocket1 from '../../assets/Images/rocket1.png';

export const Email = (props) => {
    return (
        <div className={styles.Email} id="CONTACT US">
            <div className={styles.TextInput}>
                <Title 
                    smallTitle={props.content.Type}
                    bigTitle={props.content.Title}
                    align="center"
                ></Title>
                <div className={styles.Contact}>
                    <Input change={props.change} placeholder="Email Address"/>
                    <Button width="150px" text="ANALYZE" click={props.click}/>
                </div>
            </div>
            <div className={styles.Cloud}>
                <img className={styles.rocket1} src={rocket1} alt="rocket1" />
            </div>
        </div>
    )
}
