import React from 'react'
import {NavigationItems} from '../NavigationItems/NavigationItems';
import styles from './SideDrawer.module.css';
import {Backdrop} from '../UI/Backdrop/Backdrop';
import Aux from '../../HOC/Auxilary';

export const SideDrawer = (props) => {
    let attachedClasses = [styles.SideDrawer, styles.Close];
    if(props.show){
        attachedClasses = [styles.SideDrawer, styles.Open];
    }
    return(
        <Aux>
            <Backdrop  show={props.show} exit={props.closed} ></Backdrop>
            <div className={attachedClasses.join(' ')}>
                <nav>
                    <NavigationItems/>
                </nav>
            </div>
        </Aux>
    );
}
