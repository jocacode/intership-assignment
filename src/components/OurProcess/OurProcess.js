import React from 'react'
import styles from './OurProcess.module.css';
import {ProcessCard} from './ProcessCard/ProcessCard';

export const OurProcess = (props) => {
    return (
        <div className={styles.OurProcess}>
            <div className={styles.Container}>
                <div className={styles.Photo}>
                    <img src={props.photo} alt='people'/>
                </div>
                <div className={styles.Title}>
                    <h4>{props.content.Type}</h4>
                    <h3>{props.content.Title}</h3>
                    {props.content.Cards.map(card => {
                        return <ProcessCard key={card.Type}
                            type={card.Type}
                            title={card.Title} 
                            content={card.Content}/>
                    })}
                </div>
            </div>
        </div>
    )
}
