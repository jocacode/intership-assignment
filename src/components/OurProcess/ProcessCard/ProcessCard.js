import React from 'react'
import styles from './ProcessCard.module.css';
import {ReactComponent as Blob} from '../../../assets/Blobs/blob1.svg';

export const ProcessCard = (props) => {
    let colorType = null;
    if(props.type === 'Ideas') colorType = {fill: '#edcccc'};
    else if(props.type === 'Data') colorType = {fill: '#cce0ed'};
    else if(props.type === 'Magic') colorType = {fill: '#ceedcc'};
    else colorType = {fill: 'black'};
    return (
        <div className={styles.ProcessCard}>
            <div>
                <Blob style={colorType}/>
            </div>
            <div className={styles.Title}>
                <h4>{props.title}</h4>
                <p>{props.content}</p>
            </div>
        </div>
    )
}
