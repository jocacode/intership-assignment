import React from 'react'
import styles from './Card.module.css';
import {ReactComponent as Blob} from '../../../assets/Blobs/blob1.svg'
//import {ReactComponent as Search} from '../../../assets/Blobs/search.svg'
//#edcccc      <Search className={styles.svg2}/>

export const Card = (props) => {
    let colorType = null;
    if(props.type === 'SEO') colorType = {fill: '#edcccc'};
    else if(props.type === 'Web') colorType = {fill: '#cce0ed'};
    else if(props.type === 'Social') colorType = {fill: '#ceedcc'};
    else if(props.type === 'Marketing') colorType = {fill: '#d9d5e8'};
    else colorType = {path: 'black'};

    return (
        <div className={styles.Card}>
            <div className={styles.Blob}>
                <Blob style={colorType} className={styles.svg1 }/>
            </div>
            <h4>{props.title}</h4>
            <h5>{props.content}</h5>
        </div>
    )
}
