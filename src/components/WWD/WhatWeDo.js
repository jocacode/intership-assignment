import React from 'react'
import styles from './WhatWeDo.module.css';
import {Card} from './Card/Card';
import { LoremIpsum } from 'react-lorem-ipsum';

export const WhatWeDo = (props) => {
    const cardsContent = [
        {type: 'SEO', title:'SEO Services', content: (<LoremIpsum p={1} avgWordsPerSentence={1}/>) },
        {type: 'Web', title:'Web Design', content:(<LoremIpsum p={1} avgWordsPerSentence={1}/>)},
        {type: 'Social', title:'Social Engagement', content: (<LoremIpsum p={1} avgWordsPerSentence={1}/>)},
        {type: 'Marketing', title:'Content Marketing', content: (<LoremIpsum p={1} avgWordsPerSentence={1}/>)}
    ]
    return (
        <div className={styles.WhatWeDo} id="WHAT WE DO">
            <div className={styles.Title}>
                <h5>WHAT WE DO?</h5>
                <h3>The full servie we are offering is specifically designed to meet your business needs.</h3>
            </div>
            <div className={styles.Cards}>
                {cardsContent.map(card => {
                    return <Card key={card.type} type={card.type} title={card.title} content={card.content}/>
                })}
            </div>
        </div>
    )
}
