import React from 'react'
import styles from './NavigationItem.module.css';
import {Link} from 'react-scroll';

export const NavigationItem = (props) => (
            <Link
                className={styles.NavigationItem}
                activeClass="active"
                to={props.children}
                spy={true}
                smooth={true}
                offset={-20}
                duration= {300}
            >{props.children}</Link>
);
