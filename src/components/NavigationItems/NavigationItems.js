import React from 'react'
import styles from './NavigationItems.module.css';
import {NavigationItem} from '../NavigationItems/NavigationItem/NavigationItem';

export const NavigationItems = (props) => {
    const headerItems = ['HOME', 'WHAT WE DO', 'CUSTOMER STORIES', 'PRICING', 'CONTACT US'];
    return (
        <ul className={styles.NavigationItems}>
           {headerItems.map((item, index) => {
               return <NavigationItem key={index}>{item}</NavigationItem>
           })}
        </ul>
    )
}
