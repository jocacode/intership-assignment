import React, { Component } from 'react'
import {HeaderBar} from '../HeaderBar/HeaderBar';
import Aux from '../../HOC/Auxilary';
import {SideDrawer} from '../SideDrawer/SideDrawer';

export default class Layout extends Component{

    state = {
        showSideDrawer: false,
        showHeaderBar: true
    }

    sideDrawerCloseHandler = () => {
        this.setState({showSideDrawer: false});
    }
    sideDrawerOpenHandler = () => {
        this.setState((prevState) => {
            return {showSideDrawer: !prevState.showSideDrawer}
        });
    }

    render(){
        return (
            <Aux>
                <HeaderBar showSideDrawer={this.sideDrawerOpenHandler}/>
                <SideDrawer show={this.state.showSideDrawer} closed={this.sideDrawerCloseHandler}/>
                {this.props.children}
            </Aux>
        )
    }
}
