import React from 'react'
import styles from './Customer.module.css';
import {ReactComponent as Blob} from '../../../assets/Blobs/blob1.svg'

export const Customer = (props) => {
    return (
        <div className={styles.Customer}>
            {props.text}
            <div className={styles.Profile}>
                <div className={styles.Blob}>
                    <Blob/>
                </div>
                <div>
                    <h3>{props.name} {props.surname}</h3>
                    <p>{props.position}</p>
                </div>
            </div>
        </div>
    )
}
