import React from 'react'
import styles from './Customers.module.css';
import {Title} from '../UI/Title/Title';
import {Customer} from './Customer/Customer';

export const Customers = (props) => {
    return (
        <div className={styles.Customers} id="CUSTOMER STORIES">
            <div className={styles.Container}>
                <div style={{display: 'flex', justifyContent: 'center'}}>
                    <Title align="center" smallTitle={props.title.smallTitle} bigTitle={props.title.bigTitle}/>
                </div>
                <div className={styles.Carousel}>
                        {props.customers.map((customer,index) => {
                            return <Customer 
                                key={index}
                                text={customer.Text}
                                photo={customer.Photo}
                                name={customer.Name}
                                surname={customer.Surname}
                                position={customer.Position}/>
                        })}
                </div>
            </div>
        </div>
    )
}
